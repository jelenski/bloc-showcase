import 'package:bloc_showcase/features/account/domain/repositories/user_repository.dart';
import 'package:state_graph_bloc/state_graph_bloc.dart';

import 'event.dart';
import 'state.dart';

class AccountBloc extends StateGraphBloc<AccountCommonEvent, AccountState> {
  final UserRepository _repository;

  AccountBloc(this._repository) {
    launchStream(_repository.isUserSigned, onData: (isSigned) {
      if (isSigned) {
        add(UserSignedEvent());
      } else {
        add(UserNotSignedEvent());
      }
    });
  }

  @override
  StateGraph<AccountCommonEvent, AccountState> buildGraph() {
    return StateGraph({
      LoadingState: {},
      SignedState: {},
      UnsignedState: {
        SignInEvent: sideEffect((state, event) => _repository.signIn()),
      },
    }, {
      UserSignedEvent: transition((state, event) => SignedState()),
      UserNotSignedEvent: transition((state, event) => UnsignedState()),
    });
  }

  @override
  AccountState get initialState => LoadingState();

  void signIn() => _repository.signIn();
}
