import 'package:rxdart/subjects.dart';

abstract class UserRepository {
  Stream<bool> get isUserSigned;
  void signIn();
  void logout();
  void close();
}

class UserRepositoryImpl implements UserRepository {
  final _userSignedSubject = BehaviorSubject.seeded(false);

  @override
  Stream<bool> get isUserSigned => _userSignedSubject.stream;

  @override
  void signIn() {
    _userSignedSubject.add(true);
  }

  @override
  void logout() {
    _userSignedSubject.add(false);
  }

  @override
  void close() {
    _userSignedSubject.close();
  }
}
