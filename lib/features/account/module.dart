import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

import 'domain/repositories/user_repository.dart';
import 'presentation/bloc/bloc.dart';
import 'presentation/bloc/state.dart';

final _domainModule = [
  Provider<UserRepository>(
    builder: (_) => UserRepositoryImpl(),
    dispose: (_, repository) => repository.close(),
  )
];

final _presentationModule = [
  ProxyProvider<UserRepository, AccountBloc>(
    builder: (_, repository, __) => AccountBloc(repository),
  )
];

class AccountModule extends StatelessWidget {
  final WidgetBuilder loadingScope;
  final WidgetBuilder signedScope;
  final WidgetBuilder unsignedScope;

  const AccountModule({
    Key key,
    @required this.signedScope,
    @required this.unsignedScope,
    @required this.loadingScope,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ..._domainModule,
        ..._presentationModule,
      ],
      child: BlocBuilder<AccountBloc, AccountState>(
        builder: (context, state) {
          if (state is LoadingState) {
            return loadingScope(context);
          } else if (state is SignedState) {
            return signedScope(context);
          } else if (state is UnsignedState) {
            return unsignedScope(context);
          } else {
            throw "Invalid state: $state";
          }
        },
      ),
    );
  }
}
