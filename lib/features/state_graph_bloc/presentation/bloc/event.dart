import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';

@immutable
abstract class SampleBlocEvent with EquatableMixin {}

class FetchEvent implements SampleBlocEvent {
  @override
  List<Object> get props => null;
}

class FetchCompletedEvent implements SampleBlocEvent {
  @override
  List<Object> get props => null;
}

class FetchErrorEvent implements SampleBlocEvent {
  final String message;

  FetchErrorEvent(this.message);

  @override
  List<Object> get props => [message];
}

class IncreaseEvent implements SampleBlocEvent {
  @override
  List<Object> get props => null;
}

class DecreaseEvent implements SampleBlocEvent {
  @override
  List<Object> get props => null;
}

class RetryEvent implements SampleBlocEvent {
  @override
  List<Object> get props => null;
}

class CancelEvent implements SampleBlocEvent {
  @override
  List<Object> get props => null;
}

class LogoutEvent implements SampleBlocEvent {
  @override
  List<Object> get props => null;
}
