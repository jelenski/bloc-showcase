import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';

@immutable
abstract class SampleBlocState with EquatableMixin {}

class InitialState extends SampleBlocState {
  @override
  List<Object> get props => [];
}

class LoadingState extends SampleBlocState {
  @override
  List<Object> get props => null;
}

class FetchedState extends SampleBlocState {
  FetchedState(this.counter);

  final int counter;

  @override
  List<Object> get props => [counter];

  @override
  String toString() {
    return "FetchedState($counter)";
  }
}

class FetchErrorState extends SampleBlocState {
  @override
  List<Object> get props => null;
}
