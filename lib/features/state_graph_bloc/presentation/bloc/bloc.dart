import 'dart:async';
import 'dart:core';

import 'package:bloc_showcase/common/domain/entities/snack_data.dart';
import 'package:bloc_showcase/features/account/domain/repositories/user_repository.dart';
import 'package:bloc_showcase/features/state_graph_bloc/domain/usecases/login_use_case.dart';
import 'package:bloc_showcase/features/state_graph_bloc/presentation/bloc/state.dart';
import 'package:rxdart/rxdart.dart';
import 'package:state_graph_bloc/state_graph_bloc.dart';

import 'event.dart';
export 'event.dart';
export 'state.dart';

class SampleBloc extends StateGraphBloc<SampleBlocEvent, SampleBlocState> {
  final LogInUseCase _logInUseCase;
  final UserRepository _userRepository;

  @override
  SampleBlocState get initialState => InitialState();

  final _loginSubscription = CompositeSubscription();

  SingleLiveEventSubject<SnackData> _snackSubject;
  Stream<SingleLiveEvent<SnackData>> get showSnack => _snackSubject.stream;

  SampleBloc(this._logInUseCase, this._userRepository) {
    _snackSubject = singleLiveEventSubject<SnackData>();
  }

  @override
  StateGraph<SampleBlocEvent, SampleBlocState> buildGraph() => StateGraph(
        {
          InitialState: {
            FetchEvent: transitionWithSideEffect(
              (state, event) => LoadingState(),
              (state, event) => _loginSubscription
                  .add(launchFuture(_launchLogin(), onData: add)),
            ),
          },
          LoadingState: {
            FetchCompletedEvent: transitionWithSideEffect(
              (state, event) => FetchedState(0),
              (state, event) => _snackSubject.add(SnackData(
                "Fetched!",
                SnackStatus.success,
              )),
            ),
            FetchErrorEvent: transitionWithSideEffect(
              (state, event) => FetchErrorState(),
              (state, FetchErrorEvent event) => _snackSubject.add(SnackData(
                event.message,
                SnackStatus.error,
              )),
            ),
          },
          FetchedState: {
            IncreaseEvent: transition(
              (FetchedState state, _) => FetchedState(state.counter + 1),
            ),
            DecreaseEvent: transition(
              (FetchedState state, _) => FetchedState(state.counter - 1),
            ),
          },
          FetchErrorState: {},
        },
        {
          LogoutEvent: sideEffect((state, event) => _userRepository.logout()),
          RetryEvent: _retry(),
          CancelEvent: transitionWithSideEffect(
            (state, event) {
              return InitialState();
            },
            (state, event) {
              _loginSubscription.clear();
            },
          ),
        },
      );

  Future<SampleBlocEvent> _launchLogin() {
    return _logInUseCase
        .launch()
        .then<SampleBlocEvent>((_) => FetchCompletedEvent())
        .catchError((exc) => FetchErrorEvent(exc.message ?? "Unknown error"));
  }

  StateTransitionEntry<SampleBlocState, SampleBlocEvent> _retry() =>
      transitionWithSideEffect(
        (state, event) => InitialState(),
        (state, event) => add(FetchEvent()),
      );

  Stream<String> stateAsText() => bindState((state) => state.toString());

  @override
  Stream<SampleBlocState> mapEventToState(SampleBlocEvent event) {
    return super.mapEventToState(event);
  }
}
