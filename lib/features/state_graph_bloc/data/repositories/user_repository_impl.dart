import 'package:bloc_showcase/features/state_graph_bloc/data/datasources/typicode_data_source.dart';
import 'package:bloc_showcase/features/state_graph_bloc/domain/repositories/sample_repository.dart';

class SampleRepositoryImpl implements SampleRepository {
  final TypicodeDataSource typicodeDataSource;

  SampleRepositoryImpl(this.typicodeDataSource);

  @override
  Future<void> signIn() {
    return Future.delayed(Duration(seconds: 1))
        .then((_) => typicodeDataSource.fetchTest());
  }
}
