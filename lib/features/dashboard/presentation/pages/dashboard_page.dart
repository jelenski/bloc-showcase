import 'package:bloc_showcase/features/dashboard/presentation/bloc/bloc.dart';
import 'package:bloc_showcase/features/dashboard/presentation/bloc/event.dart';
import 'package:bloc_showcase/features/dashboard/presentation/bloc/state.dart';
import 'package:bloc_showcase/features/regex_router/module.dart';
import 'package:bloc_showcase/features/state_graph_bloc/module.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:provider/provider.dart';

class DashboardPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final nestedNavigatorKey = GlobalKey<NavigatorState>();

    final bloc = Provider.of<DashboardBloc>(context);
    final stateGraphModule = StateGraphModule(title: "BLoC Showcase 1");
    final regexRouterModule = RegexRouterModule(
      navigatorKey: nestedNavigatorKey,
    );

    return BlocBuilder<DashboardBloc, DashboardState>(
      builder: (_, state) {
        return WillPopScope(
          onWillPop: () async {
            final nestedCurrentState = nestedNavigatorKey.currentState;
            if (await nestedCurrentState.maybePop()) return false;
            bloc.add(OnPopEvent());
            return state.canGoBack;
          },
          child: Scaffold(
            body: IndexedStack(
              children: <Widget>[
                stateGraphModule,
                regexRouterModule,
              ],
              index: state.currentTab.index,
            ),
            bottomNavigationBar: BottomNavigationBar(
              items: [
                BottomNavigationBarItem(
                  icon: Icon(Icons.ac_unit),
                  title: Text("StateGraphBloc"),
                ),
                BottomNavigationBarItem(
                  icon: Icon(Icons.airplay),
                  title: Text("RegexRouter"),
                ),
              ],
              currentIndex: state.currentTab.index,
              onTap: (index) =>
                  bloc.add(TabSelectedEvent(tabIndex: index, animate: true)),
            ),
          ),
        );
      },
    );
  }
}
