import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class RegexRouterIndexPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final navigator = Navigator.of(context);
    return Scaffold(
      body: Center(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            MaterialButton(
              onPressed: () => navigator
                  .pushNamed("/second/1", arguments: {"body_data", "data"}),
              child: Text("Open page with one argument"),
            ),
            MaterialButton(
              onPressed: () => navigator
                  .pushNamed("/second/1/2", arguments: {"body_data", "data"}),
              child: Text("Open page with two arguments"),
            ),
          ],
        ),
      ),
    );
  }
}
