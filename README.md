# bloc_showcase

Flutter project with libraries showcase:

- [state_graph_bloc](https://gitlab.com/marcin.jelenski/state_graph_bloc)
- [regex_router](https://gitlab.com/marcin.jelenski/regex_router)

Check a `lib/features` directory for their usage samples.